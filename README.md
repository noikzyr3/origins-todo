# Origins-todo

This is a todo project by Eric Bujeque as a test for Origins company.

## Backend

* Use Factories to simplify the unittests.
* Use Django Rest Framework.
* Use Serializers to simplify the views.
* CircleCI configured and running.
* Pep8 friendly using flake8.

## Frontend

The Frontend is made with Vue.js and Bulma. I don't usually work in frontend so I was not comfortable with any framework yet. I chose Vue.js because of it's simplicity. 

## Usage

* I create a user origins with password origins.
* A list of users in the left, if you click one of them, you can see his tasks on the right side.
* You tasks appears by default on the right side.
* Click one of your tasks and it will be marked as done.
* Click the 'X' and the task will be deleted.
* Click the 'Add new task' button and you can create a new task.
* There is a mini filter to show only not completed tasks, click the button 'false/true' to activate or deactivate it.
* Click in 'Logout' to return to the login page.
* Restriction: You can not delete other user's task.
* Restriction: You can not edit other user's task.

## Comments about the project

* I used token authentication to simplify. For production we should use a more secure authentication like JSON Web Token.
* I also used sqlite for the same reason. I always use PostgreSQL in pre production and production servers.
* In a company environment I use git-flow (master, develop, #issues-branch). 

## Personal Comments

I am not very happy with the results but I am not able to do more in this month, I am currently doing extra hours and extra days in order to deliver some projects for my company, before the end of the year.

I wanted to add some feature like tags or groups, to have a more complete backend and perform better tests. I struggled more than expected with the frontend because I have not experience with that, but I am happy to learned vue.js basics.

I used Toggl to track the time spend:
* Backend: 6 hours
* Frontend: 10 hours

## Endpoints
### Login
  
  Returns the token and user id.

* **URL**

  api/login

* **Method:**

  `POST`
  
* **Data Params**

  **Required:**
   
     `username=[string]`
     
     `password=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ token : "1234567890" }`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "Invalid credentials" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`
    
### User List
  
  Returns a users list.

* **URL**

  api/user

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{'username': 'Jon Doe', 'id': 1}]`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`
    
### Task List
  
  Returns a list for tasks for a specified user.

* **URL**

  api/tasks/:id

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{[{'title': 'Jon Doe', 'description': 'Clean Dishes', 'id': 1, 'completed': False, 'categories': ['Home', 'personal']}]`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`
    
### Add Task
  
  Creates a new task.

* **URL**

  api/task

* **Method:**

  `POST`
  
* **Data Params**

  **Required:**
   
     `title=[string]`
     
     `user=[integer]`
     
  **Not required:**
     
     `description=[string]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** `{ id : "12" }`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`

### Delete Task
  
  Delete a specific task.

* **URL**

  api/task/:id

* **Method:**

  `DELETE`

* **Success Response:**

  * **Code:** 204 <br />
    **Content:** `{[{'title': 'Jon Doe', 'description': 'Clean Dishes', 'id': 1, 'completed': False, 'categories': ['Home', 'personal']}]`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`

### Modify Task
  
  Modify a specific task.

* **URL**

  api/task/:id

* **Method:**

  `PUT`
  
* **Data Params**

  **Required:**
   
     `title=[string]`
     
     `user=[integer]`
     
  **Not required:**
     
     `description=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{[{'title': 'Jon Doe', 'description': 'Clean Dishes', 'id': 1, 'completed': False, 'categories': ['Home', 'personal']}]`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You have not permissions." }`
    
