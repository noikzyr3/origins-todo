from django.urls import path, include

urlpatterns = [
    path('api/', include('origins.api.urls')),
    path('', include('origins.frontend.urls'))
]
