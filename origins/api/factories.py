import factory

from origins.api import models


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.User
        django_get_or_create = ('username',)

    username = 'jondoe@test.com'
    email = 'jondoe@test.com'
    password = factory.PostGenerationMethodCall('set_password', 'test')


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Category
        django_get_or_create = ('name',)


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Task
        django_get_or_create = ('title', 'user')

    title = 'Test Task'
