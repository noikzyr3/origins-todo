from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from rest_framework import status

from origins.api import factories
from origins.api import models


class BaseTestCase(APITestCase):
    def setUp(self):
        self.username = 'Test@User.com'
        self.password = '12345'
        self.user = factories.UserFactory(username=self.username,
                                          password=self.password)

    def get_token(self, user=None):
        user = user or self.user
        token, _ = Token.objects.get_or_create(user=user)

        return token.key

    def login(self, token=None):
        token = token or self.get_token()
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(token)
        )


class UserTestCase(BaseTestCase):
    def setUp(self):
        super(UserTestCase, self).setUp()
        [factories.UserFactory(username='User{}'.format(n)) for n in range(3)]

    def test_user_list__ok(self):
        self.login()
        users = models.User.objects.all()
        expected = [{'username': user.username,
                    'id': user.id} for user in users]
        url = reverse('user-list')
        r = self.client.get(url)

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertListEqual(r.data, expected)

    def test_user_list__no_login(self):
        url = reverse('user-list')
        r = self.client.get(url)

        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)


class TaskTestCase(BaseTestCase):
    def setUp(self):
        self.category = factories.CategoryFactory(name='Category Test')
        super(TaskTestCase, self).setUp()
        [factories.TaskFactory(title='Task{}'.format(n), user=self.user)
         for n in range(3)]

    def test_get_tasks__ok(self):
        self.login()
        url = reverse('task-list', kwargs={'uid': self.user.id})
        r = self.client.get(url)

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 3)

    def test_get_tasks__different_user(self):
        """Different logged user can see others' tasks"""
        other_user = factories.UserFactory(username='other@user.com')
        self.login(token=self.get_token(other_user))
        url = reverse('task-list', kwargs={'uid': self.user.id})
        r = self.client.get(url)

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 3)

    def test_get_tasks__no_login(self):
        url = reverse('task-list', kwargs={'uid': self.user.id})
        r = self.client.get(url)

        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_task__ok(self):
        self.login()
        url = reverse('create-task')
        data = {'title': 'Task Test',
                'description': 'Do the unittests.',
                'user': self.user.id,
                'categories': [self.category.id]}
        r = self.client.post(url, data=data)

        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        self.assertEqual(r.data['id'], models.Task.objects.last().id)

    def test_create_task__no_login(self):
        url = reverse('create-task')
        r = self.client.post(url, {'foo': 'bar'})

        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_task__ok(self):
        self.login()
        first = models.Task.objects.first()
        url = reverse('update-task', kwargs={'pk': first.id})
        new_description = 'Another Description'
        data = {'title': first.title,
                'description': new_description,
                'user': self.user.id,
                'categories': [self.category.id]}
        r = self.client.put(url, data=data)

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(new_description,
                         models.Task.objects.get(pk=first.id).description)

    def test_update_task__no_login(self):
        url = reverse('update-task', kwargs={'pk': models.Task.objects.first().id})
        r = self.client.put(url, {'foo': 'bar'})

        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_task__other_user(self):
        other_user = factories.UserFactory(username='other@user.com')
        self.login(token=self.get_token(other_user))
        first = models.Task.objects.first()
        url = reverse('update-task', kwargs={'pk': first.id})
        new_description = 'Another Description'
        data = {'title': first.title,
                'description': new_description,
                'user': self.user.id,
                'categories': [self.category.id]}
        r = self.client.put(url, data=data)

        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(new_description,
                            models.Task.objects.get(pk=first.id).description)

    def test_delete_task__ok(self):
        self.login()
        first = models.Task.objects.first()
        url = reverse('update-task', kwargs={'pk': first.id})
        r = self.client.delete(url)

        self.assertEqual(r.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(models.Task.objects.filter(pk=first.id).exists())

    def test_delete_task__no_login(self):
        url = reverse('update-task', kwargs={'pk': models.Task.objects.first().id})
        r = self.client.delete(url, {'foo': 'bar'})

        self.assertEqual(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_task__other_user(self):
        other_user = factories.UserFactory(username='other@user.com')
        self.login(token=self.get_token(other_user))
        first = models.Task.objects.first()
        url = reverse('update-task', kwargs={'pk': first.id})
        r = self.client.delete(url)

        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
