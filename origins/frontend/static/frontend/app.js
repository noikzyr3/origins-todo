const serverUrl = 'http://18.130.247.247:8000';

let api = {
    delimiters: ['${', '}'],
    data: {
        message: 'Origins Todo',
        logintext: 'Login',
        username: null,
        password: null,
    },
    methods:{
        logout: function (e) {
            flow.token = null;
            this.logintext = "Login";
            logic.token = false;
        },
        login: function(e){
            axios
                .post(serverUrl + '/api/login', {
                    username: this.username,
                    password: this.password,
                })
                .then(function(response) {
                    flow.token = response.data.token;
                    flow.uid = response.data.uid;
                    logic.uid = response.data.uid;
                    logic.token = true;
                    app.logintext = 'Hello, ' + flow.username + '. <a v-on:click="logout" href="#"> Logout</a>'
                    flow.getUsers();
                    flow.getTasks(flow.uid)
                })
                .catch(error => console.log(error));
            e.preventDefault();
        },
        getUsers: function (e) {
            axios
                .get(serverUrl + '/api/user', {
                    headers: {'Authorization': 'Token ' + flow.token}
                })
                .then(function(response) {
                    logic.users = response.data
                })
                .catch(error => console.log(error));
        },
        getTasks: function (uid) {
            axios
                .get(serverUrl + '/api/tasks/' + uid, {
                    headers: {'Authorization': 'Token ' + flow.token}
                })
                .then(function(response) {
                    logic.tasks = response.data;
                    logic.currentUser = uid
                })
                .catch(error => console.log(error));
        },
        addTask: function (e) {
            logic.closeModal();
            axios
                .post(serverUrl + '/api/task',
                    {
                        title: logic.title,
                        description: logic.description,
                        user: flow.uid
                    },
                    {headers: {'Authorization': 'Token ' + flow.token}})
                .then(function(response) {
                    logic.getTasks(flow.uid)
                })
                .catch(error => console.log(error));
        },
        updateTask: function (title, description, completed, tid) {
            logic.closeModal();
            axios
                .put(serverUrl + '/api/task/' + tid,
                    {
                        title: title,
                        description: description,
                        user: flow.uid,
                        completed: !completed
                    },
                    {headers: {'Authorization': 'Token ' + flow.token}})
                .then(function(response) {
                    logic.getTasks(flow.uid)
                })
                .catch(error => console.log(error));
        },
        deleteTask: function (tid) {
            logic.closeModal();
            axios
                .delete(serverUrl + '/api/task/' + tid,
                    {headers: {'Authorization': 'Token ' + flow.token}})
                .then(function(response) {
                    logic.getTasks(logic.currentUser)
                })
                .catch(error => console.log(error));
        },
    }
};

let app = new Vue({
    el: '#app',
    mixins: [api],
    data: {
        message: 'Origins Todo',
        logintext: 'Login'
    },
});

let flow = new Vue({
    el: '#login',
    mixins: [api],
    data(){
        return {
            token: null,
            uid: null,
        }
    },
});

let logic = new Vue({
    el: '#logic',
    mixins: [api],
    data: {
        tid: null,
        title: null,
        description: null,
        users: null,
        tasks: null,
        token: false,
        modal: false,
        filter: false,
        currentUser: flow.uid,
        uid: null,
    },
    methods:{
        openModal: function(e){this.modal = true},
        closeModal: function(e){this.modal = false},
        showUnfinished: function () {
            this.filter = !this.filter;
            if (this.filter){
                this.tasks = this.tasks.filter(function (task) {
                    return !task.completed
                })
            }else{
                flow.getTasks(flow.uid);
            }
        }
    }
});
