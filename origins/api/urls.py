from django.urls import path

from origins.api import views


urlpatterns = [
    path('login', views.CustomAuthToken.as_view(), name='get-token'),
    path('user', views.UserListView.as_view(), name='user-list'),
    path('tasks/<int:uid>', views.TaskListView.as_view(), name='task-list'),
    path('task', views.TaskListView.as_view(), name='create-task'),
    path('task/<int:pk>', views.TaskView.as_view(), name='update-task'),
]
